define(() => 
/**
 * Class to add functions to a module to conveniently create markup language elements dynamically.
 * All created functions take as first argument the parent element that the new element shall be 
 * appended to; after that, optionally, the values for typical element attributes or text content 
 * can be passed.
 * @author Christopher Kappe <kappe@cs.uni-kl.de>
 * @date September 18, 2017
 */
class {

constructor(module, namespace)
{
	this._M = module;
	this._ns = namespace;
}

//------------------------------------------------------------------------------------------------//

/**
 * Create a function that creates an element of the given type.
 */
tagRaw(tagName)
{
	const ns = this._ns;
	this._M[tagName] = par => par.appendChild(document.createElementNS(ns, tagName));
}

/**
 * Create a function that creates an element of the given type containing the given text as 
 * child node.
 */
tagTxt(tagName)
{
	const ns = this._ns;
	this._M[tagName] = (par, str) => 
	{
		const res = par.appendChild(document.createElementNS(ns, tagName));
		if(str !== undefined)
			res.appendChild(document.createTextNode(str));
		return res;
	}
}

/**
 * Create a function *f* that creates an element of the given type and sets the specified 
 * attribute(s) if given at runtime.
 * @param first The name of the element (tag) *f* shall create.
 * @param rest The attributes that *f* accepts as arguments after the parent element.
 */
tagAttrs() { this['tagAttr' + (arguments.length - 1)](...arguments);}

// The following is hard-coded for maximum performance.

tagAttr1(tagName, aName1)
{
	const ns = this._ns;
	this._M[tagName] = (par, aVal1) => {
		const res = par.appendChild(document.createElementNS(ns, tagName))
		if(aVal1 !== undefined)
			res.setAttribute(aName1, aVal1);
		return res;
	};
}

tagAttr2(tagName, aName1, aName2)
{
	const ns = this._ns;
	this._M[tagName] = (par, aVal1, aVal2) => {
		const res = par.appendChild(document.createElementNS(ns, tagName))
		if(aVal1 !== undefined)
			res.setAttribute(aName1, aVal1);
		if(aVal2 !== undefined)
			res.setAttribute(aName2, aVal2);
		return res;
	}
}

tagAttr3(tagName, aName1, aName2, aName3)
{
	const ns = this._ns;
	this._M[tagName] = (par, aVal1, aVal2, aVal3) => {
		const res = par.appendChild(document.createElementNS(ns, tagName))
		if(aVal1 !== undefined)
			res.setAttribute(aName1, aVal1);
		if(aVal2 !== undefined)
			res.setAttribute(aName2, aVal2);
		if(aVal3 !== undefined)
			res.setAttribute(aName3, aVal3);
		return res;
	}
}

tagAttr4(tagName, aName1, aName2, aName3, aName4)
{
	const ns = this._ns;
	this._M[tagName] = (par, aVal1, aVal2, aVal3, aVal4) => {
		const res = par.appendChild(document.createElementNS(ns, tagName))
		if(aVal1 !== undefined)
			res.setAttribute(aName1, aVal1);
		if(aVal2 !== undefined)
			res.setAttribute(aName2, aVal2);
		if(aVal3 !== undefined)
			res.setAttribute(aName3, aVal3);
		if(aVal4 !== undefined)
			res.setAttribute(aName4, aVal4);
		return res;
	}
}

tagAttr5(tagName, aName1, aName2, aName3, aName4, aName5)
{
	const ns = this._ns;
	this._M[tagName] = (par, aVal1, aVal2, aVal3, aVal4, aVal5) => {
		const res = par.appendChild(document.createElementNS(ns, tagName))
		if(aVal1 !== undefined)
			res.setAttribute(aName1, aVal1);
		if(aVal2 !== undefined)
			res.setAttribute(aName2, aVal2);
		if(aVal3 !== undefined)
			res.setAttribute(aName3, aVal3);
		if(aVal4 !== undefined)
			res.setAttribute(aName4, aVal4);
		if(aVal5 !== undefined)
			res.setAttribute(aName5, aVal5);
		return res;
	}
}

tagAttr6(tagName, aName1, aName2, aName3, aName4, aName5, aName6)
{
	const ns = this._ns;
	this._M[tagName] = (par, aVal1, aVal2, aVal3, aVal4, aVal5, aVal6) => {
		const res = par.appendChild(document.createElementNS(ns, tagName))
		if(aVal1 !== undefined)
			res.setAttribute(aName1, aVal1);
		if(aVal2 !== undefined)
			res.setAttribute(aName2, aVal2);
		if(aVal3 !== undefined)
			res.setAttribute(aName3, aVal3);
		if(aVal4 !== undefined)
			res.setAttribute(aName4, aVal4);
		if(aVal5 !== undefined)
			res.setAttribute(aName5, aVal5);
		if(aVal6 !== undefined)
			res.setAttribute(aName6, aVal6);
		return res;
	}
}

tagAttr7(tagName, aName1, aName2, aName3, aName4, aName5, aName6, aName7)
{
	const ns = this._ns;
	this._M[tagName] = (par, aVal1, aVal2, aVal3, aVal4, aVal5, aVal6, aVal7) => {
		const res = par.appendChild(document.createElementNS(ns, tagName))
		if(aVal1 !== undefined)
			res.setAttribute(aName1, aVal1);
		if(aVal2 !== undefined)
			res.setAttribute(aName2, aVal2);
		if(aVal3 !== undefined)
			res.setAttribute(aName3, aVal3);
		if(aVal4 !== undefined)
			res.setAttribute(aName4, aVal4);
		if(aVal5 !== undefined)
			res.setAttribute(aName5, aVal5);
		if(aVal6 !== undefined)
			res.setAttribute(aName6, aVal6);
		if(aVal7 !== undefined)
			res.setAttribute(aName7, aVal7);
		return res;
	}
}

} );
