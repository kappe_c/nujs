define(['nu/extensibleModule', 'nu/ModuleExtender', 'nu/ViewBox'], 
/**
 * Add functions to conveniently create SVG elements dynamically.
 * @author Christopher Kappe <kappe@cs.uni-kl.de>
 * @date April 14, 2017
 */
(extensibleModule, ModuleExtender, NuViewBox) => {

const M = extensibleModule;
const ns = 'http://www.w3.org/2000/svg';
const me = new ModuleExtender(M, ns);


/**
 * SVG. 
 * @param {number[]} [vb] *viewBox* attribute, like `[origin_x, origin_y, width, height]`.
 * @param {string} [alignx='mid'] Horizontal view box alignment; 'min', 'mid' or 'max'.
 * @param {string} [aligny='mid'] Vertical view box alignment; 'min', 'mid' or 'max'.
 * @param {string} [ar='meet'] Aspect ratio behavior; 'meet' (preserve and fit), 'slice' 
 * (preserve and crop) or 'none' (distort).
 * @param {string} [background='#ffffff'] *background* style.
 * The `width` and `height` attributes are set to "100%", respectively. You may of course choose to 
 * override this explicitly using `setAttribute` or by setting the respective CSS property.
 */
M.svg = (par, vb, alignx, aligny, ar='meet', background='#ffffff') =>
{
	const res = par.appendChild(document.createElementNS(ns, 'svg'));
	// The following line seems redundant but is actually important for export (saving the SVG).
	res.setAttribute('xmlns', ns);
	res.setAttribute('xmlns:xlink', 'http://www.w3.org/1999/xlink');
	res.setAttribute('width', '100%');
	res.setAttribute('height', '100%');
	res.style.background = background;
	if(vb !== undefined)
		res.setAttribute('viewBox', vb.join(' '));
	switch(alignx) {
		case 'min': alignx = 'xMin'; break;
		case 'max': alignx = 'xMax'; break;
		default: alignx = 'xMid';
	}
	switch(aligny) {
		case 'min': aligny = 'YMin'; break;
		case 'max': aligny = 'YMax'; break;
		default: aligny = 'YMid';
	}
	if(ar === 'none')
		res.setAttribute('preserveAspectRatio', 'none');
	else
		res.setAttribute('preserveAspectRatio', alignx + aligny + ' ' + ar);
	return res;
};

/* Title (tooltip). (Not an attribute as in HTML but a child node.) */
me.tagTxt('title');

/* Group. */
me.tagAttrs('g', 'transform');

/* Definitions. */
me.tagRaw('defs');

/* Linear gradient. */
me.tagAttrs('linearGradient', 'id', 'x1', 'y1', 'x2', 'y2', 'gradientUnits', 'gradientTransform');

/* Sample point of a gradient. */
me.tagAttrs('stop', 'offset', 'stop-color', 'stop-opacity');

/* Clip path. */
me.tagAttrs('clipPath', 'id');

/* Line. */
me.tagAttrs('line', 'x1', 'y1', 'x2', 'y2');

/* Polyline. */
me.tagAttrs('polyline', 'points');

/* Polygon. */
me.tagAttrs('polygon', 'points');

/* Rectangle. */
me.tagAttrs('rect', 'x', 'y', 'width', 'height', 'rx', 'ry');

/* Ellipse. */
me.tagAttrs('ellipse', 'cx', 'cy', 'rx', 'ry');

/* Circle. */
me.tagAttrs('circle', 'cx', 'cy', 'r');


/* Text. */
me.tagAttrs('text', 'x', 'y', 'dx', 'dy');
me.tagAttrs('tspan', 'x', 'y', 'dx', 'dy');


/* Path. */
me.tagAttrs('path', 'd');


/**
 * Get a `NuViewBox` object to conveniently set and get the *viewBox* attribute.
 * @param elem {SVGElement} *svg* element to bind the `NuViewBox` object to.
 * @param [vb] {Array} [minx, miny, w, h] to set (overwrite) the initial value.
 */
M.viewBox = (elem, vb) => new NuViewBox(elem, vb);


return true; // Actually only the side effects of requiring this file are important.

} );
