define(['nu/extensibleModule', 'nu/ModuleExtender'],
/**
 * Add functions to conveniently create HTML elements dynamically.
 * @author Christopher Kappe <kappe@cs.uni-kl.de>
 * @date July 13, 2017
 */
(extensibleModule, ModuleExtender) => {

const M = extensibleModule;
const me = new ModuleExtender(M, 'http://www.w3.org/1999/xhtml');


/** Create an html element ("<html></html>"). */
M.html = () => document.createElement('html');

[
	// base
	'head', 'meta', 'title', 'body', 'div',
	// semantic structuring
	'header', 'footer', 'nav', 'aside', 'main', 'article', 'section',
	// concerning lines
	'br', 'hr', 'wbr',
	// lists
	'ul', 'ol',
].forEach(tag => me.tagAttrs(tag, 'class'));

[
	// inline box, list item
	'span', 'li',
	// text composition
	'h1', 'h2', 'h3', 'h4', 'h5', 'h6', 'p',
	// hard semantics, soft style
	'em', 'strong', 'mark', 'dfn', 'del', 'ins', 'kbd',
	// hard style, soft semantics
	'i', 'b', 'u', 's', 'sub', 'sup',
	// code
	'code', 'samp', 'pre', 'var',
	// quotes
	'blockquote', 'q'
].forEach(me.tagTxt, me);

me.tagAttrs('time', 'datetime');

// table
me.tagAttrs('table', 'sortable');
me.tagTxt('caption');
me.tagRaw('colgroup');
me.tagAttrs('col', 'span');

// table content
me.tagRaw('thead');
me.tagRaw('tbody');
me.tagRaw('tfoot');
me.tagRaw('tr');
me.tagTxt('th');
me.tagTxt('td');

// figures
me.tagRaw('figure');
me.tagRaw('picture');
me.tagAttrs('img', 'src', 'alt');
me.tagRaw('figcaption');

// forms
me.tagRaw('form');
me.tagRaw('fieldset');
me.tagTxt('legend');

// input
M.input = (par, type, value, id) => {
	const res = par.appendChild(document.createElement('input'));
	res.type = type;
	if(value !== undefined) {
		res.value = value;
		res.defaultValue = value;
	}
	if(id !== undefined)
		res.id = id;
	return res;
};
me.tagAttrs('label', 'forId');

me.tagAttrs('textarea', 'id', 'minlength', 'maxlength', 'cols', 'rows');

// select
me.tagAttrs('select', 'id');
me.tagAttrs('optgroup', 'label');
M.option = (par, value, txt, title) => {
	const res = par.appendChild(document.createElement('option'));
	if(value !== undefined) {
		res.value = value;
		if(txt === undefined)
			res.textContent = value;
	}
	if(txt !== undefined)
		res.textContent = txt;
	if(title !== undefined)
		res.title = title;
	return res;
};

// datalist (for text input element as combobox)
me.tagAttrs('datalist', 'id')

// number output
me.tagAttrs('output', 'for', 'name');
me.tagAttrs('meter', 'value', 'min', 'max');
me.tagAttrs('progress', 'value', 'max');

// misc
me.tagAttrs('a', 'href', 'target');
me.tagAttrs('link', 'rel', 'href');
me.tagRaw('address');
me.tagAttrs('abbr', 'title');

// multimedia
me.tagRaw('audio');
me.tagRaw('video');
me.tagAttrs('source', 'src', 'type');
me.tagAttrs('track', 'src', 'srclang');

// dynamic
me.tagRaw('details');
me.tagTxt('summary');
me.tagAttrs('menu', 'type', 'label');
me.tagAttrs('menuitem', 'label', 'icon');


// input types

M.inpCheckbox = (par, id, checked) => {
	const res = par.appendChild(document.createElement('input'));
	res.type = 'checkbox';
	if(id !== undefined)
		res.id = id;
	if(checked === true)
		res.checked = true;
	return res;
};

M.inpRadio = (par, name, value, checked) => {
	const res = par.appendChild(document.createElement('input'));
	res.type = 'radio';
	res.name = name;
	res.value = value;
	if(checked === true)
		res.checked = true;
	return res;
};

M.inpFile = (par, id, oninput) => {
	const res = par.appendChild(document.createElement('input'));
	res.type = 'file';
	if(id !== undefined)
		res.id = id;
	if(oninput !== undefined)
		res.oninput = oninput;
	return res;
};

M.inpNumber = (par, id, val, step, min, max) => {
	const res = par.appendChild(document.createElement('input'));
	res.type = 'number';
	if(id !== undefined)
		res.id = id;
	if(val !== undefined) {
		res.value = val;
		res.defaultValue = val;
	}
	if(step !== undefined)
		res.step = step;
	if(min !== undefined)
		res.min = min;
	if(max !== undefined)
		res.max = max;
	return res;
};

return true; // Actually only the side effects of requiring this file are important.

} );
