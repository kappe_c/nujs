define(['nu/extensibleModule', 'nu/html', 'nu/style', 'nu/svg'], 
			 (extensibleModule, html, style, svg) => 
/**
 * Object to export the public interface of the Nu library.
 * @author Christopher Kappe <kappe@cs.uni-kl.de>
 * @date September 19, 2017
 */
{

const M = extensibleModule;

/**
 * Append the string `str` as TextNode to `par`. 
 * @return The new TextNode.
 */
M.txt = (par, str) => par.appendChild(document.createTextNode(str));

// other common stuff
// M.foo;

return M;

} );
