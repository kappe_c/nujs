define(['nu/extensibleModule'], 
/**
 * This function is called once by requirejs and the result (a function that creates a new style 
 * element) is what a programmer gets when they require this module.
 * @author Christopher Kappe <kappe@cs.uni-kl.de>
 * @date April 14, 2017
 */
(extensibleModule) => {

const blockHandler = (styleElt, selector) => ({
	// Map camel case to dash.
	get: (blockRaw, property) => 
	{
		property = property.replace(/[A-Z]/g, m => '-' + m.toLowerCase());
		return blockRaw[property];
	},
	
	// Create new declarations if necessary.
	set: (blockRaw, property, newVal) => 
	{
		// camel case to dash
		property = property.replace(/[A-Z]/g, m => '-' + m.toLowerCase());
		const newDecl = newVal === null ? '' : property + ':' + newVal + ';';
		const oldVal = blockRaw[property];
		if(oldVal !== undefined)
		{
			if(newVal === null)
				delete blockRaw[property];
			if(oldVal === newVal)
				return true;
			else
				blockRaw[property] = newVal;
			// Replace the old value. 
			// Notice that the selector may contain special RexExp characters like ".".
			const sel = selector.replace(/[-/\\^$*+?.()|[\]{}]/g, '\\$&');
			const oldBlockStr = styleElt.textContent.match(new RegExp(sel + ' {.*?}\n'))[0];
			const oldDecl = property + ':' + oldVal + ';';
			const newBlockStr = oldBlockStr.replace(oldDecl, newDecl);
			styleElt.textContent = styleElt.textContent.replace(oldBlockStr, newBlockStr);
		}
		else if(newVal !== null) // null here would mean deleting a non-existing property.
		{
			// Add new declaration.
			blockRaw[property] = newVal;
			styleElt.textContent = styleElt.textContent.replace(selector + ' {', selector + ' {' + newDecl);
		}
		return true;
	}
});

const rulesHandler = styleElt => ({
	// Create new declaration blocks if necessary.
	get: (rulesRaw, selector) => {
		if(selector in rulesRaw)
			return rulesRaw[selector];
		styleElt.appendChild(document.createTextNode(selector + ' {}\n'));
		return rulesRaw[selector] = new Proxy({}, blockHandler(styleElt, selector));
	},
	// If an object is given, create a new Proxy like above and apply all the declarations.
	set: (rulesRaw, selector, obj) => {
		if(typeof obj !== 'object') {
			console.error('You cannot define a declaration block by anything but an object');
			return false;
		}
		const block = styleElt.rules[selector]; // utilizing the above `get` method
		for(let property in obj)
			block[property] = obj[property];
		return true;
	}
});

/**
 * Create a new style node under `par`.
 * @return Style element that has an additional `rules` property which is an object like 
 * `{ selector1: { property1: value1, property2: value2 }, selector2: etc }`.
 */
extensibleModule.style = par => {
	const res = par.appendChild(document.createElement('style'));
	res.rules = new Proxy({}, rulesHandler(res));
	return res;
};

return true; // Actually only the side effects of requiring this file are important.

} );
