define(() =>
/**
 * Class to conveniently get and set (the parameters of) a *viewBox* attribute.
 * @author Christopher Kappe <kappe@cs.uni-kl.de>
 * @date July 18, 2018
 */
class NuViewBox {

/**
 * @param elem {SVGElement} *svg* element to bind this object to.
 * @param [vb] {Array} Data like [minx, miny, w, h] to set (overwrite) the initial value.
 */
constructor(elem, vb)
{
  this._elem = elem;
  if(vb !== undefined)
    elem.setAttribute('viewBox', vb.join(' '));
}

/** Get the view box as an array like `[minx, miny, w, h]`. */
get data() { return this._elem.getAttribute('viewBox').split(' ').map(str => parseFloat(str)); }

/** Set the view box with an array like `[minx, miny, w, h]`. */
set data(arr) { this._elem.setAttribute('viewBox', arr.join(' ')); }

// This class could be extended to allow setting getting the individual values, or allow more
// high-level functionality like pan and zoom, or a camera shake, etc.

});
