require(['nu/nu'], (nu) =>
{
	console.log('Loaded Nu');
	
	const rls = nu.style(document.head).rules;
	rls.body.background = 'lightblue';
	rls['#cenDiv'].display = 'flex';
	rls['header, nav, main, aside, footer'].border = '0.1em solid black';
	rls['header, nav, main, aside, footer'].margin = '0.3em';
	rls['header, nav, main, aside, footer'].padding = '0.5em';
	
	const body = document.body;
	nu.txt(nu.header(body), 'header');
	const cenDiv = nu.div(body);
	cenDiv.id = 'cenDiv';
	nu.txt(nu.footer(body), 'footer');
	
	const nav = nu.nav(cenDiv);
	nav.id = 'nav';
	nu.txt(nav, 'nav');
	const navList = nu.ul(nav);
	nu.txt(nu.a(nu.li(navList), '#main'), 'to the main');
	nu.txt(nu.a(nu.li(navList), '#'), 'to the top');
	nu.txt(nu.a(nu.li(navList), 'https://www.ixquick.com/', '_blank'), 'ixquick');
	
	const main = nu.main(cenDiv);
	main.id = 'main';
	nu.txt(main, 'main');
	nu.hr(main);
	
	const artcle = nu.article(main);
	nu.txt(artcle, 'article');
	nu.h2(artcle, 'Article Heading');
	nu.p(artcle, 'p');
	const sec1 = nu.section(artcle);
	nu.h3(sec1, 'Section1 heading');
	nu.p(sec1, 'p');
	
	const aside = nu.aside(cenDiv);
	nu.txt(aside, 'aside');
	const asideList = nu.ol(aside);
	nu.em(nu.li(asideList), 'em');
	nu.strong(nu.li(asideList), 'strong');
	nu.mark(nu.li(asideList), 'mark');
	nu.dfn(nu.li(asideList), 'dfn');
	nu.del(nu.li(asideList), 'del');
	nu.ins(nu.li(asideList), 'ins');
	nu.kbd(nu.li(asideList), 'kbd');
} );
