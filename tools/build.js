{
	baseUrl: '../www',
// 	dir: '../www-built',
// 	skipDirOptimize: true
	name: 'nu/nu',
// 	include: [],
	out: '../nu.js',
// 	modules: [{ 'name': 'nu/nu' }],
	optimize: 'none',
// 	optimize: 'closure',
// 	closure: { CompilerOptions: {
// 		languageIn:  Packages.com.google.javascript.jscomp.CompilerOptions.LanguageMode.valueOf('ECMASCRIPT6_STRICT'),
// 		languageOut: Packages.com.google.javascript.jscomp.CompilerOptions.LanguageMode.valueOf('ECMASCRIPT5')
// 	} }
	// We want an anonymous module so that the path of the "nu.js" file can be used in require 
	// calls of web apps that use this library.
	onBuildWrite: (moduleName, path, contents) => moduleName !== 'nu/nu' ?
		contents : contents.replace("define('nu/nu',[", "define([")
}
