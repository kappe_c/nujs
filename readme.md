# Usage

The library should be loaded using requirejs. It is a really thin layer on top of the functions 
already existing in JavaScript. 
New (nu) DOM elements can be created by functions who are named like the respective tag 
(HTML or SVG).

The first argument is always the parent element to which the new element shall be added, 
then optionally the element-specific attributes like *src* and *alt* for *img* elements or 
*x*, *y*, *width* and *height* for *rect* elements. Finally, where it makes sense 
(for example for the *em* tag), a string can be passed that will be appended to the new element 
as a text node.

The *svg* element is a little special; its second (optional) argument is an array specifying the 
view box, like `[origin_x, origin_y, width, height]`. Its *width* and *height* attributes are set 
to "100%" by default so that it adapts to a parent container (the aspect ratio of 
the graphical content will be preserved by default). These values can be changed of course or 
overridden by the respective CSS properties (see below).

```javascript
const box = nu.div(document.body);
nu.h1(box, 'Nu');
nu.h2(box, 'new html, new style, new svg');
const svg = nu.svg(box, [-80, -45, 160, 90]);
nu.rect(svg, -70, -40, 140, 80);
nu.circle(svg, 0, 0, 70);
```

The returned objects are untouched compared to the ones created by the JavaScript API; 
so one can write for example `nu.div(document.body).style.padding = '0.5%';` or 
`nu.div(document.body).setAttribute('class', 'box');`.
The only exception to this is *style* elements. They are added a `rules` property which can be 
used to add CSS rules, as in the following snippet.

```javascript
const sty = nu.style(document.head);
const rls = sty.rules;
rls.div.width = '100%'; // Simple selectors as "div" can be accessed via dot syntax.
rls['.textBoxes'].textAlign = 'center'; // Class selectors need brackets and quotation marks.
rls.circle = { // It is possible to assign a block of style declarations in one statement.
	fill: 'blue',
	strokeWidth: 4,
	stroke: 'black'
};
```

More example code can be found in the file *www/main.js*.

Currently no standalone documentation has been compiled for the API; one has to look at the code 
and the (JSDoc) comments, particularly in the files *www/nu/html.js* and *www/nu/svg.js*. 
This is not much and quite easy to comprehend.


# Test Website

A simple website using Nu is also part of this repository. To view it, open the file 
*www/index.html* in a web browser. However, the JavaScript code by which it is created is 
certainly more interesting (see the file *www/main.js*).


# Technical Information

Published under the LGPL license, this web project contains the following files and directories:

- tools/ - Build tools to optimize the project.
- www/ - Web assets for the project.
    - nu/ - Directory to store the Nu library scripts.
    - index.html - Entry point into the test website.
    - main.js - Top-level script used by index.html.
    - require.js - JS library for dependency management that is used here.
- license.txt - Text of the LGPL which governs this project.
- package.json - Package configuration.
- readme.md - This readme file.

To build the library, run:
```
node tools/r.js -o tools/build.js
```

This build command creates an optimized version of the project in the file nu.js 
(in the same directory as this readme file) which will be optimized to include all of its 
dependencies.

For more information on the optimizer:
[http://requirejs.org/docs/optimization.html](http://requirejs.org/docs/optimization.html)

For more information on using requirejs:
[http://requirejs.org/docs/api.html](http://requirejs.org/docs/api.html)

The latest stable version of the built library is also directly available under *Downloads*.


# TODO

While the library has already been tested quite thoroughly by the author and should be 
pretty much free of bugs, it is at most in a beta state concerning the public API. 
In particular, not all HTML and SVG elements can be created so far (like the ones not yet or only 
recently standardized). 

The unnamed function arguments are a distinctive feature of this library which allow for 
concise code (`rect(x, y, width, height)` vs `rect({x: x, y: y, width: width, height: height})`), 
but this requires a fixed selection and order of the respective attributes. 
For elements for which this is not so clear as in the case of *rect*, the API may very well be 
subject to change.

Some more high-level functionality may also be added in the future. 
The `nu.inpCheckbox`, `nu.inpFile` and `nu.inpNumber` functions are already a tiny step in this 
direction.
